within CSVUtilities;
function csvStringFromString "Create CSV string from String vector, delimiter and number of significant digits"
  extends Modelica.Icons.Function;
  input String vector[:] "String input vector";
  input String delimiter = ",";
  input Integer significantDigits = 6 "Number of significant digits";
  output String result "CSV string";
algorithm
  // Start with empty string
  result :="";
  if size(vector, 1) == 0 then
    result := "";
  else
    result := vector[1];
    for i in 2:size(vector, 1) loop
      result := result + delimiter + vector[i];
    end for;
  end if;
end csvStringFromString;
