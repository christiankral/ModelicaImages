within CSVUtilities;
model TestWriteCSV "Test block for writing to CSV file"
  extends Modelica.Icons.Example;
  WriteCSV writeCSV(
    useHeader=true,
    header={"ramp"},
    useTime=true,
    samplePeriod=0.1) annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.Ramp ramp(height=1, duration=1) annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
equation
  connect(ramp.y, writeCSV.u[1]) annotation (Line(points={{-39,10},{-20,10},{-20,0},{-12,0}}, color={0,0,127}));
end TestWriteCSV;
