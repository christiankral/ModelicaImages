within CSVUtilities;
block WriteCSV "Simple block to write CSV files with one time and data column"
  extends Modelica.Blocks.Icons.Block;
  parameter String fileName = "output.csv";
  parameter Integer nin=1 "Number of inputs";
  parameter Boolean useHeader = false "Enables header, if true";
  parameter String header[:] = fill("",nin) "Header string; time string needs to be included manually, if useTime = true" annotation(Dialog(enable = useHeader));
  parameter Boolean useTime = false "Use time as first column, if true";
  parameter String delimiter = "," "Delimiter";
  parameter Modelica.SIunits.Time startTime = 0 "Start time";
  parameter Modelica.SIunits.Time samplePeriod( start = 0.1) "Sample time";
  parameter Boolean append = false "Append file, if true";
  parameter Integer significantDigits = 6 "Number of significant digits";
  Modelica.Blocks.Interfaces.RealInput u[nin] "Connector of Real input signals" annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
protected
  output Boolean sampleTrigger "True, if sample time instant";
  output Boolean firstTrigger(start = false, fixed = true) "Rising edge signals first sample instant";
algorithm
  sampleTrigger := sample(startTime, samplePeriod);
  when sampleTrigger then
    firstTrigger := time <= startTime + samplePeriod / 2;
    if firstTrigger then
      // This branch is only executed upon the first trigger event
      // Delete file if append == false
      if not append then
        Modelica.Utilities.Files.remove(fileName);
      end if;
      // Write header strings, if useHeader == True
      if useHeader then
          // if useTime then
          //   // Write "time" as first header, then the remaining headers
          //   Modelica.Utilities.Streams.print(Utilities.csvStringFromString(cat(1,{"time"},header), delimiter = delimiter, significantDigits = significantDigits), fileName);
          // else
          //   // Write headers
        Modelica.Utilities.Streams.print(CSVUtilities.csvStringFromString(
          header,
          delimiter=delimiter,
          significantDigits=significantDigits), fileName);
          // end if;
      end if;
    end if;

    // Write data
    if useTime then
      // Write time as first column
      Modelica.Utilities.Streams.print(CSVUtilities.csvStringFromReal(
        cat(
          1,
          {time},
          u),
        delimiter=delimiter,
        significantDigits=significantDigits), fileName);
    else
      Modelica.Utilities.Streams.print(CSVUtilities.csvStringFromReal(
        u,
        delimiter=delimiter,
        significantDigits=significantDigits), fileName);
    end if;
  end when;
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false), graphics={  Text(extent={{-60,-58},{60,62}},      lineColor = {0, 0, 0}, textString = "CSV")}),
    Diagram(coordinateSystem(preserveAspectRatio = false)),
    Documentation(info="<html>
<p>The sampled input data of this blocks are written to a CSV file.</p>
</html>"));
end WriteCSV;
