within CSVUtilities;
function csvStringFromReal "Create CSV string from Real vector, delimiter and number of significant digits"
  extends Modelica.Icons.Function;
  input Real vector[:] "Data vector";
  input String delimiter = ",";
  input Integer significantDigits = 6 "Number of significant digits";
  output String result "CSV string";
algorithm
  // Start with empty string
  result :="";
  if size(vector, 1) == 0 then
    result := "";
  else
    result := String(vector[1], significantDigits = significantDigits);
    for i in 2:size(vector, 1) loop
      result := result + delimiter + String(vector[i], significantDigits = significantDigits);
    end for;
  end if;
end csvStringFromReal;
