within ;
package ModelicaImages "Images of the Modelica Standard Library"
  extends Modelica.Icons.Package;

  annotation (uses(Modelica(version="3.2.3")));
end ModelicaImages;
