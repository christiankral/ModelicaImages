using PyPlot

rc("text",usetex=true);
rc("text.latex",preamble="\\usepackage{fourier}\\usepackage[scaled=0.95]{inconsolata}");
rc("font", family="serif")

t1=collect(-0.2:0.1:0);
t2=collect(0:0.001:1);
t3=collect(1:0.1:1.2);

wMin=0.1;
wMax=1;

w1=wMin*ones(size(t1));
w1L = zeros(size(t1));
w2 = 10 .^ (log10(wMin) .+ (log10(wMax) - log10(wMin)) .* min.(1, t2/1));
w2L = t2;
w3 = wMax*ones(size(t3));
w3L = w3;

f=3.8/3.3
figure(figsize=(3.3*f,2.8))
semilogy(t1,w1,color="blue",linestyle="-",linewidth=1)
semilogy(t2,w2,color="blue",linestyle="-",linewidth=1)
semilogy(t3,w3,color="blue",linestyle="-",linewidth=1)
xlim(-0.2,1.2)
ylim(0.09,1.1)
yticks([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],[L"$\texttt{magnitude1}$","","","","","","","","",L"$\texttt{magnitude2}$"])
xticks([0,1],[L"$\texttt{startTime}$",L"$\texttt{startTime+duration}$"])
grid(true)
subplots_adjust(left=1-0.8/f, right=1-0.15/f, top=0.95, bottom=0.20)
xlabel(L"$\rightarrow~\texttt{time}$")
ylabel(L"$\rightarrow~\texttt{magnitude}$")

savefig("ComplexRampPhasorLog.png",dpi=96)

f=3.8/3.3
figure(figsize=(3.3*f,2.8))
plot(t1,w1L,color="blue",linestyle="-",linewidth=1)
plot(t2,w2L,color="blue",linestyle="-",linewidth=1)
plot(t3,w3L,color="blue",linestyle="-",linewidth=1)
xlim(-0.2,1.2)
yticks([0.0,0.2,0.4,0.6,0.8,1],[L"$\texttt{magnitude1}$","","","","",L"$\texttt{magnitude2}$"])
ylim(-0.05,1.05)
xticks([0,1],[L"$\texttt{startTime}$",L"$\texttt{startTime+duration}$"])
subplots_adjust(left=1-0.8/f, right=1-0.15/f, top=0.95, bottom=0.20)
grid(true)
xlabel(L"$\rightarrow~\texttt{time}$")
ylabel(L"$\rightarrow~\texttt{magnitude}$")

savefig("ComplexRampPhasorLinear",dpi=96)
