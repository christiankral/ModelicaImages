using DelimitedFiles, PyPlot, Base

rc("text",usetex=true);
rc("text.latex",preamble="\\usepackage{fourier}\\usepackage[scaled=0.95]{inconsolata}");
rc("font", family="serif")

CSV = readdlm("SaturatingInductor.csv", ',', Float64, '\n')
i = CSV[:,1]
lact = CSV[:,2]
psi = CSV[:,3]

lzer = lact[1]
# Assume that Lzer / Lnom = Lnom / Linf
linf = 1 / lzer

function delta(x,y,dx,der)
    x1 = x
    y1 = y
    x2 = x1 + dx
    y2 = y1 + der * dx
    if dx > 0
        fill([x1,x2,x2],[y1,y1,y2],color="#C0C0C0")
    else
        fill([x1,x1,x2],[y1,y2,y2],color="#C0C0C0")
    end
end

f=3.8/3.3
close("all")
figure(figsize=(3.3*f,2.8))
plot(i,lact,color="blue",linestyle="-",linewidth=1)
xticks([0,1],[L"$0$",L"$\texttt{Inom}$"])
yticks([0,linf,1,lzer],[L"$0$",L"$\texttt{Linf}$",L"$\texttt{Lnom}$",L"$\texttt{Lzer}$"])
grid(true)
subplots_adjust(left=1-0.8/f, right=1-0.15/f, top=0.95, bottom=0.20)
xlabel(L"$\rightarrow~\texttt{i}$")
ylabel(L"$\rightarrow~\texttt{Lact}$")

savefig("SaturatingInductor_Lact_i.png",dpi=96)
savefig("SaturatingInductor_Lact_i_tight.png",dpi=96,bbox_inches="tight")

f=3.8/3.3
figure(figsize=(3.3*f,2.8))
plot(i,psi,color="blue",linestyle="-",linewidth=1)
# Plot derivative Lzer
psizer = psi[end]
izer = psizer / lzer
plot([0,izer],[0,psizer],color="black",linestyle="--",linewidth=1)
text(0,psizer*0.9,L"$\frac{\Delta \texttt{Psi}}{\Delta \texttt{i}} = \texttt{Lzer}$",backgroundcolor = "white")
delta(0.25,0.25*lzer,0.12,lzer)
# Plot derivative Linf
psiinf2 = psi[end]
iinf2 = i[end]
psiinf1 = psi[end] - linf * iinf2
iinf1 = 0
plot([iinf1,iinf2],[psiinf1,psiinf2],color="black",linestyle="--",linewidth=1)
text(iinf2,psizer*0.61,L"$\frac{\Delta \texttt{Psi}}{\Delta \texttt{i}} = \texttt{Linf}$",backgroundcolor = "white",ha = "right")
delta(iinf2-0.5,psiinf2-linf*0.5,-1,linf)
xticks([0,1],[L"$0$",L"$\texttt{Inom}$"])
yticks([0,1],[L"$0$",L"$\texttt{Lnom} \cdot \texttt{Inom}$"])
grid(true)
subplots_adjust(left=1-0.8/f, right=1-0.15/f, top=0.95, bottom=0.20)
xlabel(L"$\rightarrow~\texttt{i}$")
ylabel(L"$\rightarrow~\texttt{Psi}$")

savefig("SaturatingInductor_Psi_i.png",dpi=96)
