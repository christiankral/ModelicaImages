within ModelicaImages.Electrical.Analog.Basic.SaturatingInductor;
model SaturatingInductor
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter Integer Variant=1;
  parameter Modelica.SIunits.Inductance LNominal=1 "Nominal inductance at nominal current";
  parameter Modelica.SIunits.Current INominal=1 "Nominal current";
  parameter Modelica.SIunits.Inductance LZero=4*LNominal "Inductance at small current";
  parameter Modelica.SIunits.Inductance LInf=LNominal/4 "Inductance at large current";
  parameter Modelica.SIunits.Current IMax=4*INominal "Source current (peak)";
  Modelica.Electrical.Analog.Sources.RampCurrent rampCurrent(
    I=IMax,
    duration=1,
    offset=0)
    annotation (Placement(transformation(
        origin={-60,-40},
        extent={{10,-10},{-10,10}},
        rotation=270)));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-70,-80},{-50,-60}})));
  Modelica.Electrical.Analog.Basic.SaturatingInductor saturatingInductor(
    Inom=INominal,
    Lnom=LNominal,
    Lzer=LZero,
    Linf=LInf,
    i(fixed=false)) annotation (Placement(transformation(
        origin={0,-40},
        extent={{-10,-10},{10,10}},
        rotation=270)));

  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (Placement(transformation(extent={{-40,-10},{-20,-30}})));
  CSVUtilities.WriteCSV writeCSV(
    fileName="SaturatingInductor.csv",
    nin=3,
    samplePeriod=1E-3) annotation (Placement(transformation(extent={{70,40},{90,60}})));
  Modelica.Blocks.Sources.RealExpression expressionLact(y=saturatingInductor.Lact) annotation (Placement(transformation(extent={{-20,20},{0,40}})));
  Modelica.Blocks.Sources.RealExpression expressionPsi(y=saturatingInductor.Psi) annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Blocks.Math.Gain gainI(k=1/INominal) annotation (Placement(transformation(extent={{20,50},{40,70}})));
  Modelica.Blocks.Math.Gain gainLact(k=1/LNominal) annotation (Placement(transformation(extent={{20,20},{40,40}})));
  Modelica.Blocks.Math.Gain gainPsi(k=1/(LNominal*INominal)) annotation (Placement(transformation(extent={{20,-10},{40,10}})));
equation
  connect(rampCurrent.p, ground.p)
    annotation (Line(points={{-60,-50},{-60,-60}},
                                               color={0,0,255}));
  connect(ground.p, saturatingInductor.n)
    annotation (Line(points={{-60,-60},{0,-60},{0,-50}},
                                                       color={0,0,255}));
  connect(rampCurrent.n, currentSensor.p) annotation (Line(points={{-60,-30},{-60,-20},{-40,-20}}, color={0,0,255}));
  connect(currentSensor.n, saturatingInductor.p) annotation (Line(points={{-20,-20},{0,-20},{0,-30}}, color={0,0,255}));
  connect(currentSensor.i, gainI.u) annotation (Line(points={{-30,-9},{-30,60},{18,60}}, color={0,0,127}));
  connect(expressionLact.y, gainLact.u) annotation (Line(points={{1,30},{18,30}}, color={0,0,127}));
  connect(expressionPsi.y, gainPsi.u) annotation (Line(points={{1,0},{18,0}}, color={0,0,127}));
  connect(gainI.y, writeCSV.u[1]) annotation (Line(points={{41,60},{60,60},{60,48.6667},{68,48.6667}}, color={0,0,127}));
  connect(gainLact.y, writeCSV.u[2]) annotation (Line(points={{41,30},{60,30},{60,50},{68,50}}, color={0,0,127}));
  connect(gainPsi.y, writeCSV.u[3]) annotation (Line(points={{41,0},{60,0},{60,51.3333},{68,51.3333}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}})),
    experiment(Interval=0.0001, Tolerance=1e-06),
    Documentation(info="<html>
<p>This simple circuit uses the saturating inductor which has an inductance dependent on current.</p>
<p>Plot <code>saturatingInductor.Psi</code> and <code>saturatingInductor.Lact</code> versus <code>saturatingInductor.i</code></p>
</html>"));
end SaturatingInductor;
